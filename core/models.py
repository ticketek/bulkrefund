from django.db import models
from .utils import TixPayHandler
import datetime
# Cmireate your models here.
import csv


VISA_MESSAGE_WIDTHS = (
    ("libre", 3),  # dejar espacios
    ("tarjeta", 16),
    ("libre2", 1),  # espacio
    ("cupon", 8),  # zero padded izquierda, TICKET_NUMBER
    ("fecha", 6),  # FECHA DE TRANSACCIOn
    ("libre3", 3),  # dejar espacios
    ("auth", 5),  # auth_code
    ("importe", 15),  # contiene los decimales
    ("cuotas", 2),  # Dejar esapcios
    ("libre4", 21),  # dejar espacios
)

MERCHANT_TRANSLATOR = {"19548346": {"banco": "17", "sucursal": "490"},
                       "19548379": {"banco": "316", "sucursal": "13"},
                       "22174650": {"banco": "7", "sucursal": "246"},
                       "25174814": {"banco": "34", "sucursal": "10"},
                       "30173645": {"banco": "316", "sucursal": "51"},
                       "93735066": {"banco": "316", "sucursal": "13"},
                       "19546464": {"banco": "17", "sucursal": "490"},
                       "93735082": {"banco": "316", "sucursal": "13"},
                       }


VISA_HEADER_WIDTHS = (('marca', 1),  # "siempre 0
                     ('service', 2),  # SIEMPRE 00
                     ("fecha", 6),  # FORMATO DDMMAA -> fecha DE HOY
                     ("banco", 3),  # ME los pasa ari
                     ("sucursal", 3),  # me los pasa ari
                     ("lote", 4),  # siempre es 0001, 0002
                     ("libre", 7),  # siempre espacios
                     ("codigo_transaction", 4),  # siempre 6000
                     ("establecimiento", 10),
                     # merchant1 y despues merchant2 (poner una opcion de que tipo de merchant se quiere exportar)
                     ("comprobante", 2),  # CANTIDAD DE COMPROVANTES no puede haber mas de 99 registros por lote # NOTA IMPORTANTE
                     ("importe", 15),  # incluye 2 decimales IMPORTE TOTAL DEL LOTE
                     ("cuotas", 1),  # espacio
                     ("caja", 4),  # completar con 4 ceros
                     ("marca2", 1),  # dejar un espacio
                     ("libre2", 17),  # dejar espacios
                     )

class Transaction(models.Model):
    batch = models.CharField(max_length=255)
    external_id = models.CharField(max_length=255)
    ccn = models.CharField(max_length=17)
    card_name = models.CharField(max_length=50)
    expiry = models.CharField(max_length=5, null=True, blank=True)
    account_number = models.CharField(max_length=20)
    amount = models.CharField(max_length=50)
    amount2 = models.CharField(max_length=50, null=True, blank=True)
    seller = models.CharField(max_length=30)
    installments = models.CharField(max_length=10)
    merchant = models.CharField(max_length=30)
    merchant2 = models.CharField(max_length=30)
    ticket_number = models.CharField(max_length=30)
    ticket_number2 = models.CharField(max_length=30)
    batch_number = models.CharField(max_length=30)
    batch_number2 = models.CharField(max_length=30)
    name = models.CharField(max_length=255)
    hphone = models.CharField(max_length=255)
    ophone = models.CharField(max_length=255)
    auth_code = models.CharField(max_length=255, null=True, blank=True)
    auth_code2 = models.CharField(max_length=255, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.external_id

    @property
    def full_refund_message(self):
        return self.softix_message(full_refund=True)

    @property
    def partial_refund_message(self):
        return self.softix_message()


    @classmethod
    def offline_visa_refund_message_header(cls, batch, merchant, size, total_amount):
        merchant = merchant.replace('NP', '')
        data = {"marca": "0",
                "service": "00",
                "fecha": datetime.datetime.today().strftime('%d%m%y'),
                "banco": MERCHANT_TRANSLATOR[merchant]['banco'], # me lo manda ariel
                "sucursal": MERCHANT_TRANSLATOR[merchant]['sucursal'], # me lo manda ariel
                "lote": str(batch),
                "libre": "       ",
                "codigo_transaction": "0005",
                "establecimiento": merchant,
                "comprobante": str(size),
                "importe": str(total_amount).replace('.', '') + '0',
                "cuotas": " ",
                "caja": "0000",
                "marca2": " ",
                "libre2": "                 ",
        }
        return ''.join([data[field_name].rjust(width, '0')
                        for field_name, width in VISA_HEADER_WIDTHS])

    def offline_visa_refund_message_item(self, type):
        data = {"libre": "   ",
                "tarjeta": self.ccn,
                "libre2": " ",
                "cupon": self.ticket_number if type == 1 else self.ticket_number2,
                "fecha": datetime.datetime.strptime(self.external_id.split(',')[0], '%y%m%d').strftime('%d%m%y'),
                "libre3": "   ",
                "auth": self.auth_code[1:] if type == 1 else self.auth_code2[1:],
                "importe": self.get_parsed_amount(type),
                "cuotas": "  ",
                "libre4": "                     "}

        return ''.join([data[field_name].rjust(width, '0')
                        for field_name, width in VISA_MESSAGE_WIDTHS]), self.amount if type == 1 else self.amount2
    @classmethod
    def offline_master_refund_header(self):
        return ['COD AUT', 'Cupon', 'Fecha Op.', 'Tarjeta Completa', 'tipo de tarjeta', 'Comercio', 'Cuotas', 'Importe',
                'Nota/Motivo']

    def offline_master_refund_item(self, type):
        return [self.auth_code if type == 1 else self.auth_code2,
                self.ticket_number if type == 1 else self.ticket_number2,
                datetime.datetime.strptime(self.external_id.split(',')[0], '%y%m%d').strftime('%d-%m-%y'),
                self.ccn,
                'CREDITO',
                (self.merchant if type == 1 else self.merchant2).replace('NP', ''),
                self.installments,
                self.amount if type == 1 else self.amount2,
                'Procesar Venta / error interno ya solucionado.']

    def get_parsed_amount(self, type):
        amount = self.amount if type == 1 else self.amount2
        return amount.replace('.', '')


    def softix_message(self, full_refund=False):
        message = """bytes:00000 site:EZE1 point:66666 seller:QTROG1 please:  edc_trans=2 status=17 ccn="{}" card_name="{}" xpr="{}" seller="{}" auth="CREDIT" an={} edcname="" edcpasswd="" format="" amt={} amt2={} cuotas={} cuota_plan="0" cvc="" merchant_id="{}" merchant_id2="{}" ticket_num1={} ticket_num2={} batch_num1={} batch_num2={} request_cnt=451 name2="" name3="{}" hphone="{}" ophone="{}" dob="00000000" add1="" add2="" add3="" angel_num=1""".format(self.ccn,
                                                                                  self.card_name,
                                                                                  self.expiry if self.expiry else "",
                                                                                  self.seller,
                                                                                  self.account_number,
                                                                                  self.amount.replace('.', ''),
                                                                                  self.amount2.replace('.', '') if self.amount2 and full_refund else 0,
                                                                                  self.installments,
                                                                                  self.merchant,
                                                                                  self.merchant2,
                                                                                  self.ticket_number,
                                                                                  self.ticket_number2,
                                                                                  self.batch_number,
                                                                                  self.batch_number2,
                                                                                  self.name,
                                                                                  self.hphone,
                                                                                  self.ophone)
        message = message.replace('bytes:00000',  'bytes:%5.5d' % len(message))
        return message

    def softix_ack(self, ticket_number):
        message = """edc_trans=6 ticket_number={} angel_num=1""".format(ticket_number)
        return message.replace('bytes:00000',  'bytes:%5.5d' % len(message))

    def refund(self, full_refund=False):
        tixpay = TixPayHandler()

        refund_payload = tixpay.refund(self.softix_message(full_refund=full_refund))
        if refund_payload['valid']:
            tixpay.ack(self.softix_ack(refund_payload['data']['ticket_num1']))

        TransactionResponse.objects.create(transaction=self,
                                           status=refund_payload['data']['status'],
                                           status_message=refund_payload['data']['message'],
                                           code=refund_payload['data'].get('code'),
                                           code2=refund_payload['data'].get('code2'),
                                           ref=refund_payload['data'].get('ref'),
                                           ref2=refund_payload['data'].get('ref2'),
                                           ticket_number=refund_payload['data'].get('ticket_num1'),
                                           ticket_number2=refund_payload['data'].get('ticket_num2'),
                                           batch_number=refund_payload['data'].get('batch_num1'),
                                           batch_number2=refund_payload['data'].get('batch_num2'))
        tixpay.close()


class TransactionResponse(models.Model):
    transaction = models.ForeignKey('core.Transaction', on_delete=models.CASCADE, related_name='responses')
    status = models.CharField(max_length=50)
    status_message = models.CharField(max_length=255)
    code = models.CharField(max_length=50, null=True, blank=True)
    code2 = models.CharField(max_length=50, null=True, blank=True)
    ref = models.CharField(max_length=50, null=True, blank=True)
    ref2 = models.CharField(max_length=50, null=True, blank=True)
    ticket_number = models.CharField(max_length=50, null=True, blank=True)
    ticket_number2 = models.CharField(max_length=50, null=True, blank=True)
    batch_number = models.CharField(max_length=50, null=True, blank=True)
    batch_number2 = models.CharField(max_length=50, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

