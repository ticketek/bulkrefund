from core.models import Transaction
from celery import shared_task


@shared_task
def bulk_refund(transactions, full_refund=False):
    for transaction in transactions:
        refund.delay(transaction, full_refund)

@shared_task
def refund(transaction, full_refund):
    tr = Transaction.objects.get(id=transaction)
    return tr.refund(full_refund=full_refund)