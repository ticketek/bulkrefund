from django.contrib import admin
from .models import Transaction, TransactionResponse
# Register your models here.
from django import forms
from django.urls import path
from io import TextIOWrapper
from django.shortcuts import redirect, render
import csv
from django.contrib import messages
from .tasks import bulk_refund as async_refund
from django.http import HttpResponse

from django.core.paginator import Paginator

from django.contrib.admin import SimpleListFilter

class StatusFilter(SimpleListFilter):
    title = 'status' # or use _('country') for translated title
    parameter_name = 'status_message'

    def lookups(self, request, model_admin):
        status = set([c['status_message'] for c in TransactionResponse.objects.all().values('status_message')])
        return [(c, c) for c in status]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(responses__status_message=self.value())

class TransactionImportForm(forms.Form):
    file = forms.FileField()
    batch = forms.CharField()

class TransactionResponseAdmin(admin.TabularInline):
    model = TransactionResponse
    extra = 0
    readonly_fields = ['status', 'status_message', 'code', 'code2', 'ref', 'ref2', 'ticket_number', 'ticket_number2',
                       'batch_number', 'batch_number2',  'created', 'updated']


class TransactionAdmin(admin.ModelAdmin):
    readonly_fields = ('batch', 'external_id', "seller", "ccn", "card_name", "expiry", "account_number", "amount", "amount2", "installments",
                       "merchant", "merchant2", "ticket_number", "ticket_number2", "batch_number", "batch_number2",
                       "name", "ophone", 'hphone', 'created', 'updated', 'full_refund_message', 'partial_refund_message')
    inlines = [TransactionResponseAdmin]
    list_display = ['external_id', 'batch', 'card_name', 'merchant', 'ticket_number', 'last_response']
    search_fields = ['external_id']
    list_filter = ['merchant', 'card_name', 'batch', StatusFilter]
    date_hierarchy = 'created'
    actions = ['refund', 'full_refund', 'export_offline_visa1', 'export_offline_visa2', 'export_offline_master1', 'export_offline_master2']
    change_list_template = "core/admin/transaction_changelist.html"

    def get_queryset(self, request):
        qs = super(TransactionAdmin, self).get_queryset(request)
        return qs

    def last_response(self, obj):
        try:
            return TransactionResponse.objects.filter(transaction=obj).reverse()[0].status_message
        except:
            return ''
        last_response.admin_order_field = 'last_response'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            f = TextIOWrapper(request.FILES['file'].file, encoding=request.encoding)
            reader = csv.reader(f, delimiter='|')
            for line in reader:
                transaction, created = Transaction.objects.get_or_create(
                                                                batch=request.POST['batch'],
                                                                seller=line[1],
                                                                ccn=line[2],
                                                                card_name=line[3],
                                                                expiry=line[4],
                                                                account_number=line[5],
                                                                amount=line[6],
                                                                amount2=line[7],
                                                                installments=line[8],
                                                                merchant=line[9],
                                                                merchant2=line[10],
                                                                ticket_number=line[11],
                                                                ticket_number2=line[12],
                                                                batch_number=line[13],
                                                                batch_number2=line[14],
                                                                name=line[15],
                                                                ophone=line[16],
                                                                hphone=line[17],
                                                                external_id=line[0])
                transaction.auth_code = line[18]
                transaction.auth_code2 = line[19]
                transaction.save()
            self.message_user(request, "Your csv file has been imported")
            return redirect("..")
        form = TransactionImportForm()
        payload = {"form": form}
        return render(
            request, "csv_form.html", payload
        )
    def export_offline_visa1(self, request, queryset):
        return self.export_offline_visa(request, queryset, 1)

    export_offline_visa1.short_description = "export VISA merchant 1"

    def export_offline_visa2(self, request, queryset):
        return self.export_offline_visa(request, queryset, 2)  
    export_offline_visa2.short_description = "export VISA merchant 2"

    def export_offline_master1(self, request, queryset):
        return self.export_offline_mastercard(request, queryset, 1)
    export_offline_master1.short_description = "export MASTERCARD merchant 1"

    def export_offline_master2(self, request, queryset):
        return self.export_offline_mastercard(request, queryset, 2)
    export_offline_master2.short_description = "export MASTERCARD merchant 2"


    def export_offline_mastercard(self, request, queryset, type):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; export_mastercard.csv'
        paginator = Paginator(queryset, 99)
        writer = csv.writer(response, delimiter=';')
        data = [Transaction.offline_master_refund_header()]
        for i in range(paginator.num_pages):
            for transaction in iter(paginator.get_page(i)):
                data.append(transaction.offline_master_refund_item(type))
        writer.writerows(data)
        return response

    def export_offline_visa(self, request, queryset, type):
        paginator = Paginator(queryset, 99)
        transaction_set = dict()
        for i in range(paginator.num_pages):
            batch = i+1
            total_amount = 0
            transaction_set[i] = []
            for transaction in iter(paginator.get_page(i)):
                i_transaction, amount = transaction.offline_visa_refund_message_item(type)
                total_amount += float(amount)
                transaction_set[i].append(i_transaction)
            merchant = transaction.merchant if type == 1 else transaction.merchant2
            header = Transaction.offline_visa_refund_message_header(batch, merchant, len(transaction_set[i]), total_amount)
            transaction_set[i].insert(0, header)
        data = []
        for key in transaction_set:
            for line in transaction_set[key]:
                data.append(line)
        response = HttpResponse('\n'.join(data), content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=export.txt'

        return response

    def refund(self, request, queryset):
        ids = list(set([transaction.id for transaction in queryset]))
        async_refund.delay(ids, full_refund=False)
        self.message_user(request, 'Devoluciones SIN FEE generadas con exito', messages.SUCCESS)

    refund.short_description = "Devolver SIN fee"

    def full_refund(self, request, queryset):
        ids = list(set([transaction.id for transaction in queryset]))
        async_refund.delay(ids, full_refund=True)
        self.message_user(request, 'Devoluciones CON FEE generadas con exito', messages.SUCCESS)
    full_refund.short_description = "Devolver CON fee"

admin.site.register(Transaction, TransactionAdmin)

