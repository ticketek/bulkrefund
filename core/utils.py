from django.conf import settings
from .socket_handler import SimpleSocket
import re
RSP_PATTERN = r'(\w+)=(["][^"]*["]|\S+)'

class TixPayHandler(object):

    def __init__(self):
        self.tixpay_connection = SimpleSocket(host=settings.TIXPAY_ADDRESS,
                                              port=settings.TIXPAY_PORT)

    def refund(self, message):
        data = {"valid": False, "data": ""}
        self.tixpay_connection.send(message)
        response = self.tixpay_connection.receive(gpay=True).decode()
        print('RECEIVED : {}'.format(response))
        data['data'] = self.parse_response(response)
        if 'Aprobada' in response:
            data['valid'] = True
        return data

    def parse_response(self, response):
        response, message = response.split('&')
        response = response.split('please:')[1]
        regex = re.compile(RSP_PATTERN)
        tvalues = dict(regex.findall(response))
        tvalues['message'] = message
        return tvalues

    def ack(self, message):
        self.tixpay_connection.send(message)


    def close(self):
        self.tixpay_connection.close()