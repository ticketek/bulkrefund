from django.test import TestCase
from core.models import Transaction
# Create your tests here.
transaction = Transaction.objects.get_or_create(
external_id="201022,106",
seller="QTROG1",
ccn="376458450901000",
card_name="American Express",
expiry="1223",
account_number="5030389",
amount="34300",
amount2="5000",
installments=1,
merchant="NP9906752993",
merchant2="NP9906558879",
ticket_number="2026",
ticket_number2="2977",
batch_number="343",
batch_number2=330,
name="Ariel Barrios",
ophone="1234",
hphone="1234")