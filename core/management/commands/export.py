from django.core.management.base import BaseCommand, CommandError
import csv
from core.models import Transaction, TransactionResponse

class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('/tmp/export.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            transactions = Transaction.objects.all().values('id', 'batch', 'external_id', 'ccn', 'card_name', 'expiry',
                                         'account_number', 'amount', 'amount2', 'seller', 'installments',
                                         'merchant', 'merchant2', 'ticket_number', 'ticket_number2', 'batch_number', 'batch_number2', 'name',
                                         'hphone', 'ophone')
            for transaction in transactions:
                not_aprobada = ''
                aprobada = TransactionResponse.objects.filter(transaction__id=transaction['id'], status_message__in=[' Aprobada', 'Aprobada']).exists()
                if not aprobada:
                    not_aprobada = ';'.join([tr['status_message'] for tr in TransactionResponse.objects.filter(transaction__id=transaction['id']).values('status_message')])
                data = [transaction['id'], transaction['batch'], transaction['external_id'], transaction['ccn'], transaction['card_name'], transaction['expiry'],
                                         transaction['account_number'], transaction['amount'],transaction['amount2'], transaction['seller'], transaction['installments'],
                                         transaction['merchant'], transaction['merchant2'], transaction['ticket_number'], transaction['ticket_number2'], transaction['batch_number'], transaction['batch_number2'], transaction['name'],
                                         transaction['hphone'], transaction['ophone']]
                data.append('Aprobada' if aprobada else not_aprobada)
                print(data)
                writer.writerow(data)
