# -*- coding: utf-8 -*-

import socket


class SocketHandler(object):
    """
    Friendly Socket Handler.
    """

    def __init__(self, *args, **kwargs):
        """
        Params:
            host (str)
            port (int)
            timeout (int) optional, default 60
        """

        self._host = kwargs['host']
        self._port = int(kwargs['port'])
        self._timeout = kwargs.get('timeout', 60)

        self._socket = None

    # support the context manager protocol
    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.close()
        # don't suppress the raised exception, if any
        return False

    def open(self):
        """
        Open the conection.
        """

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.settimeout(self._timeout)
        self._socket.connect((self._host, self._port))

    def close(self):
        """
        Close the connection.
        """
        try:
            self._socket.close()
        except Exception as e:
            pass

    def send(self, message):
        """
        Send a message.
        """

        try:
            self.open()
        except:
            pass

        msg_len = len(message)
        total_sent = 0
        print('SENT : {}'.format(message))
        self._socket.sendall(message.encode())

    def receive(self):
        """
        Receive a message. This method must be overwritten to implement
        the recv strategy.
        """

        raise NotImplementedError('receive method must be overwritten.')


class SimpleSocket(SocketHandler):
    def receive(self, gpay=False):
        End = '<EOF>'
        total_data = [];
        data = ''
        while True:
            data = self._socket.recv(8192)
            if gpay:
                return data
            if End in data:
                total_data.append(data[:data.find(End)])
                break
            total_data.append(data)
            if len(total_data) > 1:
                # check if end_of_data was split
                last_pair = total_data[-2] + total_data[-1]
                if End in last_pair:
                    total_data[-2] = last_pair[:last_pair.find(End)]
                    total_data.pop()
                    break
        total_data = ''.join(total_data)
        return total_data

